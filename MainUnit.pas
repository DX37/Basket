unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TMainForm = class(TForm)
    ScoreLabel: TLabel;
    GameButton: TButton;
    LeftMove: TButton;
    RightMove: TButton;
    Ring: TImage;
    Basket: TImage;
    GameTimer: TTimer;
    LeftDownHole: TImage;
    RightDownHole: TImage;
    RightUpHole: TImage;
    LeftUpHole: TImage;
    LeftHole: TImage;
    RightHole: TImage;
    HolesTimer: TTimer;
    procedure HolesSwitcher(HoleToCheck: integer);
    procedure FileLoader(Records: string);
    procedure FileSaver(Records: string);
    procedure FormCreate(Sender: TObject);
    procedure GameTimerTimer(Sender: TObject);
    procedure GameButtonClick(Sender: TObject);
    procedure HolesTimerTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//������������� ����������
var
  //������� �����
  MainForm: TMainForm;
  f: file of integer;
  //���������� ���� boolean, ����������� ��� ��������
  GameMode, BasketPlace: boolean;
  ScoreCheck: record
    Check: boolean;
    One: boolean;
    Score: integer;
    Interval: integer;
  end;
  //������, ������� �������� ���������� ���� boolean, ����������� ��� ��������
  HoleCheck: record
    YellowIsON: boolean;
    RedIsON: boolean;
    Check: boolean;
    OneShot: boolean;
  end;
  BasketHole: record
    LH: boolean;
    RH: boolean;
    LDH: boolean;
    LUH: boolean;
    RDH: boolean;
    RUH: boolean;
  end;
  //��������� ���������� ���� integer, ����������� ��� ���������� ��������
  LeftHoleB, RightHoleB, LeftDownHoleB, LeftUpHoleB, RightDownHoleB, RightUpHoleB: integer;
  //���������� ������ ����������, �� ������� �������� ��������
  BasketX, BasketY: integer;
  //������ ����������, �� ������� �������� ��������
  Rad: integer;
  //����������-���� ��������
  dfi, fi: real;
  //����������-������ ��� ���������� ���� � ������������
  red, yellow, white: string;
  //�������������� ����������
  HoleSwitcher, Score: integer;

implementation

{$R *.dfm}

//��������� ������� �� ������ "������ ����"
procedure TMainForm.GameButtonClick(Sender: TObject);
begin
  if GameMode = false then
    begin
      GameButton.Caption:= '�������������';
      GameTimer.Enabled:= true;
      HolesTimer.Enabled:= true;
      LeftMove.Enabled:= true;
      RightMove.Enabled:= true;
      GameMode:= true;
    end
  else
    begin
      GameButton.Caption:= '������ ����';
      GameTimer.Enabled:= false;
      HolesTimer.Enabled:= false;
      LeftMove.Enabled:= false;
      RightMove.Enabled:= false;
      GameMode:= false;
    end;
end;

//��������� ���������� ���������� � var ������������� ��������,
//� ����� ������������ �����������
procedure FormCreateMain;
begin
  //���������� ���������� string ����� �� ��������
  red:= 'img\hole\red.bmp';
  yellow:= 'img\hole\yellow.bmp';
  white:= 'img\hole\white.bmp';
  //����� ���������� �����
  MainForm.ScoreLabel.Caption:= 'Score: '+IntToStr(Score);
  //���������� ������ ����������, �� ������� �������� ��������
  BasketX:= 187;
  BasketY:= 187;
  //������ ����������, �� ������� �������� ��������
  Rad:= 150;
  //���� ��������
  fi:= 0;
  dfi:= 0.1;
  ScoreCheck.Score:= 1500;
  ScoreCheck.Interval:= 1500;
  //�������������� ����������, ����������� ��� ��������
  HoleCheck.Check:= false;
  HoleCheck.RedIsON:= false;
  HoleCheck.YellowIsON:= false;
  HoleCheck.OneShot:= false;
  //����������, ���������� �������� ��������� ���������
  LeftHoleB:= 0;
  RightHoleB:= 0;
  LeftDownHoleB:= 0;
  LeftUpHoleB:= 0;
  RightDownHoleB:= 0;
  RightUpHoleB:= 0;
  //���������� ��� ������������ ��������� ������
  GameMode:= false;
  //������ �������� ���������� � ������ � � ����������
  MainForm.Basket.Transparent:= true;
  MainForm.Basket.Left:= 33;
  MainForm.Basket.Top:= 179;
  //���������� ������ ��� ������������ ��������
  MainForm.LeftMove.Enabled:= false;
  MainForm.RightMove.Enabled:= false;
  //������������ �����������
  MainForm.LeftHole.Picture.LoadFromFile(white);
  MainForm.RightHole.Picture.LoadFromFile(white);
  MainForm.LeftUpHole.Picture.LoadFromFile(white);
  MainForm.LeftDownHole.Picture.LoadFromFile(white);
  MainForm.RightUpHole.Picture.LoadFromFile(white);
  MainForm.RightDownHole.Picture.LoadFromFile(white);
end;

//��������� �������� �����, ���������� ��������� FormCreateMain
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FileSaver(ExtractFilePath(Application.ExeName) + 'BasketSave.dat');
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  GameTimer.Enabled:= false;
  HolesTimer.Enabled:= false;
  if FileExists(ExtractFilePath(Application.ExeName) + 'BasketSave.dat') then
  if (MessageBox(MainForm.Handle,
    PChar('������ �� �� ��������� ����������� ����?'),
    PChar('���� "��������"'), MB_YESNO or MB_ICONQUESTION)) = mrYes
  then FileLoader(ExtractFilePath(Application.ExeName) + 'BasketSave.dat')
  else Score:= 1000
  else Score:= 1000;
  FormCreateMain;
  randomize;
end;

//������� ������
procedure TMainForm.GameTimerTimer(Sender: TObject);
begin
  //���� ���� ��������� �� ������ ������, �� �������� ��������� ������
  if RightMove.MouseInClient then
    begin
      Basket.Top:= BasketX+round(Rad*cos(fi));
      Basket.Left:= BasketY-round(Rad*sin(fi));
      fi:= fi+dfi;
    end;
  //���� ���� ��������� �� ����� ������, �� �������� ��������� �����
  if LeftMove.MouseInClient then
    begin
      Basket.Top:= BasketX+round(Rad*cos(fi));
      Basket.Left:= BasketY-round(Rad*sin(fi));
      fi:= fi-dfi;
    end;

  //���������� ������� ScoreLabel ���������� �����
  ScoreLabel.Caption:= 'Score: '+IntToStr(Score);

  //�������� ��������� ��������
  if ((LeftHole.Left > Basket.Left) and (LeftHole.Left + LeftHole.Width < Basket.Left + Basket.Width) and
  (LeftHole.Top > Basket.Top) and (LeftHole.Top + LeftHole.Height < Basket.Top + Basket.Height))
  then BasketHole.LH:= true else BasketHole.LH:= false;

  if ((RightHole.Left > Basket.Left) and (RightHole.Left + RightHole.Width < Basket.Left + Basket.Width) and
  (RightHole.Top > Basket.Top) and (RightHole.Top + RightHole.Height < Basket.Top + Basket.Height))
  then BasketHole.RH:= true else BasketHole.RH:= false;

  if ((LeftUpHole.Left > Basket.Left) and (LeftUpHole.Left + LeftUpHole.Width < Basket.Left + Basket.Width) and
  (LeftUpHole.Top > Basket.Top) and (LeftUpHole.Top + LeftUpHole.Height < Basket.Top + Basket.Height))
  then BasketHole.LUH:= true else BasketHole.LUH:= false;

  if ((RightUpHole.Left > Basket.Left) and (RightUpHole.Left + RightUpHole.Width < Basket.Left + Basket.Width) and
  (RightUpHole.Top > Basket.Top) and (RightUpHole.Top + RightUpHole.Height < Basket.Top + Basket.Height))
  then BasketHole.RUH:= true else BasketHole.RUH:= false;

  if ((LeftDownHole.Left > Basket.Left) and (LeftDownHole.Left + LeftDownHole.Width < Basket.Left + Basket.Width) and
  (LeftDownHole.Top > Basket.Top) and (LeftDownHole.Top + LeftDownHole.Height < Basket.Top + Basket.Height))
  then BasketHole.LDH:= true else BasketHole.LDH:= false;

  if ((RightDownHole.Left > Basket.Left) and (RightDownHole.Left + RightDownHole.Width < Basket.Left + Basket.Width) and
  (RightDownHole.Top > Basket.Top) and (RightDownHole.Top + RightDownHole.Height < Basket.Top + Basket.Height))
  then BasketHole.RDH:= true else BasketHole.RDH:= false;

  //�������� ������� �������� ��������� � ������� �������� � ����
  if (BasketHole.LH = true) and (LeftHoleB = 2) and (HoleCheck.RedIsON = true)
  then BasketPlace:= true

  else if (BasketHole.RH = true) and (RightHoleB = 2) and (HoleCheck.RedIsON = true)
  then BasketPlace:= true

  else if (BasketHole.LUH = true) and (LeftUpHoleB = 2) and (HoleCheck.RedIsON = true)
  then BasketPlace:= true

  else if (BasketHole.LDH = true) and (LeftDownHoleB = 2) and (HoleCheck.RedIsON = true)
  then BasketPlace:= true

  else if (BasketHole.RUH = true) and (RightUpHoleB = 2) and (HoleCheck.RedIsON = true)
  then BasketPlace:= true

  else if (BasketHole.RDH = true) and (RightDownHoleB = 2) and (HoleCheck.RedIsON = true)
  then BasketPlace:= true
  else BasketPlace := false;

  //�������� ������� �������� ���������, �������� ����� ���� � �������� ����,
  //������������� �� ����
  if (BasketPlace = true) and (ScoreCheck.Check = false)
  then
    begin
      Score:= Score+100;
      ScoreCheck.Check:= true;
    end;

  //���� �������� �� ��������� ����� �������� ���������, �� ����������� ���
  if (HoleCheck.RedIsON = true) and (BasketPlace = false) and (ScoreCheck.Check = false)
  then
    begin
      Score:= Score-200;
      ScoreCheck.Check:= true;
    end;

  //������� ���������, ����� �������� ���� ������������
  if Score <= 0 then
    begin
      GameTimer.Enabled:= false;
      HolesTimer.Enabled:= false;
      ShowMessage('�� ���������!');
      GameButton.Caption:= '������ ����';
      FormCreateMain;
      Score:= 1000;
    end;


  if (Score >= ScoreCheck.Score) then
    begin
      HolesTimer.Interval:= ScoreCheck.Interval;
      ScoreCheck.One:= true;
      ScoreCheck.Score:= ScoreCheck.Score+100;
      ScoreCheck.Interval:= ScoreCheck.Interval-100;
    end;
end;

//������ ���������
procedure TMainForm.HolesTimerTimer(Sender: TObject);
begin
  if (HoleCheck.OneShot = false) then
  begin
    HoleSwitcher:= random(6);
    HolesSwitcher(HoleSwitcher);
    HoleCheck.OneShot:= true;
    ScoreCheck.Check:= false;
  end
else
  for HoleSwitcher:= 0 to 5 do
  begin
    HolesSwitcher(HoleSwitcher);
  end;

  //���� �� ������������ ������� ���������, �� �������� ����,
  //��� ���� ��������� ���� ������������� false
  if (HoleCheck.RedIsON = false) and (HoleCheck.YellowIsON = false) then HoleCheck.OneShot:= false;
end;

procedure TMainForm.HolesSwitcher(HoleToCheck: integer);
begin
  HoleCheck.Check:= false;
  case HoleToCheck of
    //�������� ����� ��� ������ ������. ������ �� ������.
    0:begin
      //���� ��� ��������� - 0, � �������� - false, ��:
      if (HoleCheck.OneShot = false) and (LeftHoleB = 0) and (HoleCheck.Check = false) and (HoleCheck.YellowIsON = false) then
        begin
          //�������� �������� ������ �� �����
          LeftHole.Picture.LoadFromFile(yellow);
          //��������� ����������� ��� 1
          LeftHoleB:= 1;
          //�������� ������������� ��� true
          //(���� ������� �������� ���������� ��������� ��������)
          HoleCheck.Check:= true;
          HoleCheck.YellowIsON:= true;
        end;
      //���� ��� ��������� - 1, � �������� - false,
      //� ��� ����������� �������� ���������, ��:
      if (LeftHoleB = 1) and (HoleCheck.Check = false) and (HoleCheck.RedIsON = false)
      and (HoleCheck.YellowIsON = true) then
        begin
          //�������� �������� ������ �� �������
          LeftHole.Picture.LoadFromFile(red);
          //��������� ����������� ��� 2
          LeftHoleB:= 2;
          //�������� ������������� ��� true
          //(��� �������� ���������� ��������� ��������)
          HoleCheck.Check:= true;
          //�������� ������� �������� ��������� ������������� true
          HoleCheck.RedIsON:= true;
          HoleCheck.YellowIsON:= false;
        end;
      //���� ��� ��������� - 2, � �������� - false, ��:
      if (LeftHoleB = 2) and (HoleCheck.Check = false) then
        begin
          //�������� �������� ������ �� �����
          LeftHole.Picture.LoadFromFile(white);
          //��������� ����������� ��������� ��� - 0
          LeftHoleB:= 0;
          //�������� ������������� ��� true
          //(���� ������� �������� ���������� ��������� ��������)
          HoleCheck.Check:= true;
          //�������� ������� �������� ��������� ������������� false
          HoleCheck.RedIsON:= false;
        end;
    end;
    1:begin
      if (HoleCheck.OneShot = false) and (RightHoleB = 0) and (HoleCheck.Check = false) and (HoleCheck.YellowIsON = false) then
        begin
          RightHole.Picture.LoadFromFile(yellow);
          RightHoleB:= 1;
          HoleCheck.Check:= true;
          HoleCheck.YellowIsON:= true;
        end;
      if (RightHoleB = 1) and (HoleCheck.Check = false) and (HoleCheck.RedIsON = false)
      and (HoleCheck.YellowIsON = true) then
        begin
          RightHole.Picture.LoadFromFile(red);
          RightHoleB:= 2;
          HoleCheck.Check:= true;
          HoleCheck.RedIsON:= true;
          HoleCheck.YellowIsON:= false;
        end;
      if (RightHoleB = 2) and (HoleCheck.Check = false) then
        begin
          RightHole.Picture.LoadFromFile(white);
          RightHoleB:= 0;
          HoleCheck.Check:= true; 
          HoleCheck.RedIsON:= false;
        end;
    end;
    2:begin
      if (HoleCheck.OneShot = false) and (LeftUpHoleB = 0) and (HoleCheck.Check = false) and (HoleCheck.YellowIsON = false) then
        begin
          LeftUpHole.Picture.LoadFromFile(yellow);
          LeftUpHoleB:= 1;
          HoleCheck.Check:= true;
          HoleCheck.YellowIsON:= true;
        end;
      if (LeftUpHoleB = 1) and (HoleCheck.Check = false) and (HoleCheck.RedIsON = false)
      and (HoleCheck.YellowIsON = true) then
        begin
          LeftUpHole.Picture.LoadFromFile(red);
          LeftUpHoleB:= 2;
          HoleCheck.Check:= true;
          HoleCheck.RedIsON:= true;
          HoleCheck.YellowIsON:= false;
        end;
      if (LeftUpHoleB = 2) and (HoleCheck.Check = false) then
        begin
          LeftUpHole.Picture.LoadFromFile(white);
          LeftUpHoleB:= 0;
          HoleCheck.Check:= true;  
          HoleCheck.RedIsON:= false;
        end;
    end;
    3:begin
      if (HoleCheck.OneShot = false) and (LeftDownHoleB = 0) and (HoleCheck.Check = false) and (HoleCheck.YellowIsON = false) then
        begin
          LeftDownHole.Picture.LoadFromFile(yellow);
          LeftDownHoleB:= 1;
          HoleCheck.Check:= true;
          HoleCheck.YellowIsON:= true;
        end;
      if (LeftDownHoleB = 1) and (HoleCheck.Check = false) and (HoleCheck.RedIsON = false)
      and (HoleCheck.YellowIsON = true) then
        begin
          LeftDownHole.Picture.LoadFromFile(red);
          LeftDownHoleB:= 2;
          HoleCheck.Check:= true;
          HoleCheck.RedIsON:= true;
          HoleCheck.YellowIsON:= false;
        end;
      if (LeftDownHoleB = 2) and (HoleCheck.Check = false) then
        begin
          LeftDownHole.Picture.LoadFromFile(white);
          LeftDownHoleB:= 0;
          HoleCheck.Check:= true;       
          HoleCheck.RedIsON:= false;
        end;
    end;
    4:begin
      if (HoleCheck.OneShot = false) and (RightUpHoleB = 0) and (HoleCheck.Check = false) and (HoleCheck.YellowIsON = false) then
        begin
          RightUpHole.Picture.LoadFromFile(yellow);
          RightUpHoleB:= 1;
          HoleCheck.Check:= true;
          HoleCheck.YellowIsON:= true;
        end;
      if (RightUpHoleB = 1) and (HoleCheck.Check = false) and (HoleCheck.RedIsON = false)
      and (HoleCheck.YellowIsON = true) then
        begin
          RightUpHole.Picture.LoadFromFile(red);
          RightUpHoleB:= 2;
          HoleCheck.Check:= true;
          HoleCheck.RedIsON:= true;
          HoleCheck.YellowIsON:= false;
        end;
      if (RightUpHoleB = 2) and (HoleCheck.Check = false) then
        begin
          RightUpHole.Picture.LoadFromFile(white);
          RightUpHoleB:= 0;
          HoleCheck.Check:= true;    
          HoleCheck.RedIsON:= false;
        end;
    end;
    5:begin
      if (HoleCheck.OneShot = false) and (RightDownHoleB = 0) and (HoleCheck.Check = false) and (HoleCheck.YellowIsON = false) then
        begin
          RightDownHole.Picture.LoadFromFile(yellow);
          RightDownHoleB:= 1;
          HoleCheck.Check:= true;
          HoleCheck.YellowIsON:= true;
        end;
      if (RightDownHoleB = 1) and (HoleCheck.Check = false) and (HoleCheck.RedIsON = false)
      and (HoleCheck.YellowIsON = true) then
        begin
          RightDownHole.Picture.LoadFromFile(red);
          RightDownHoleB:= 2;
          HoleCheck.Check:= true;
          HoleCheck.RedIsON:= true;
          HoleCheck.YellowIsON:= false;
        end;
      if (RightDownHoleB = 2) and (HoleCheck.Check = false) then
        begin
          RightDownHole.Picture.LoadFromFile(white);
          RightDownHoleB:= 0;
          HoleCheck.Check:= true;     
          HoleCheck.RedIsON:= false;
        end;
    end;
  end;
end;

procedure TMainForm.FileLoader(Records: string);
begin
  AssignFile(f, Records);
  Reset(f);
  Read(f, Score);
  CloseFile(f);
end;

procedure TMainForm.FileSaver(Records: string);
begin
  AssignFile(f, Records);
  Rewrite(f);
  Write(f, Score);
  CloseFile(f);
end;

end.
